package it.fullstack.sdk.core;

/**
 * Profili
 */
public class SDKProfiles {
  /**
   * Profilo local
   */
  public static final String LOCAL = "sdk-local";
  /**
   * Profilo sviluppo
   */
  public static final String SVILUPPO = "sdk-sviluppo";
  /**
   * Profilo validazione
   */
  public static final String VALIDAZIONE = "sdk-validazione";
  /**
   * Profilo produzione
   */
  public static final String PRODUZIONE = "sdk-produzione";
}
