package it.fullstack.sdk.core;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Calendar;
import java.util.Locale;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

/**
 * Stampa le info dell'Applicazione
 */
@Slf4j
@Component
public class ApplicationStartupLogger implements InitializingBean {

  @Autowired
  private Environment env;
   
  @Override
  public void afterPropertiesSet() throws Exception {
    String applicationName = env.getProperty("spring.application.name", "[NOT PROVIDED]");
    String javaVersion = System.getProperty("java.version","[NOT AVAILABLE]");
    String protocol = env.getProperty("server.ssl.key-store") == null ? "http" : "https";
    String serverPort = env.getProperty("server.port", "8080");
    String contextPath = env.getProperty("server.servlet.context-path", "/");
    String hostAddress = "localhost";
    try {
        hostAddress = InetAddress.getLocalHost().getHostAddress();
    } catch (UnknownHostException e) {
        log.warn("The host name could not be determined, using `localhost` as fallback");
    }
    log.info("\n----------------------------------------------------------\n\t" +
            "Application '{}' is running!:\n\t" +
            "Profile(s): \t{}\n\t" +
            "Java: \t\t{}\n\t" +
            "Local: \t\t{}://localhost:{}{}\n\t" +
            "External: \t{}://{}:{}{}\n\t" +
            "Locale: \t{}\n\t" +
            "TimeZone: \t{}\n" +
            "----------------------------------------------------------",
        applicationName,
        env.getActiveProfiles(),
        javaVersion,
        protocol,
        serverPort,
        contextPath,
        protocol,
        hostAddress,
        serverPort,
        contextPath,
        Locale.getDefault(),
        Calendar.getInstance().getTimeZone()
    );
  }

}
