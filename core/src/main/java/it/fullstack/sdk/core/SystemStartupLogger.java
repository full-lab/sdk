package it.fullstack.sdk.core;

import java.text.CharacterIterator;
import java.text.StringCharacterIterator;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

/**
 * Stampa le info dell'Applicazione
 */
@Slf4j
@Component
public class SystemStartupLogger implements InitializingBean {

  @Override
  public void afterPropertiesSet() throws Exception {
	  Runtime runtime = Runtime.getRuntime();
	  log.info(
	        "\n----------------------------------------------------------\n\t" +
	        "System Resources: \n\t" +
        	"Memory Max:{} \n\t" +
        	"Memory Total:{} \n\t" +
        	"Memory Free:{} \n\t" +
			"Available Processors:{}" + 
	        "\n----------------------------------------------------------",
	        humanReadableByteCountBin(runtime.maxMemory()),
	        humanReadableByteCountBin(runtime.totalMemory()),
	        humanReadableByteCountBin(runtime.freeMemory()),
	        runtime.availableProcessors()
	);
  }
	  
  public static String humanReadableByteCountBin(long bytes) {
	    long absB = bytes == Long.MIN_VALUE ? Long.MAX_VALUE : Math.abs(bytes);
	    if (absB < 1024) {
	        return bytes + " B";
	    }
	    long value = absB;
	    CharacterIterator ci = new StringCharacterIterator("KMGTPE");
	    for (int i = 40; i >= 0 && absB > 0xfffccccccccccccL >> i; i -= 10) {
	        value >>= 10;
	        ci.next();
	    }
	    value *= Long.signum(bytes);
	    return String.format("%.1f %ciB", value / 1024.0, ci.current());
  }

}
