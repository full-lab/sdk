package it.fullstack.sdk.core.exception;

import java.util.Map;

public class ApiErrorResponseDTO {
    private String code;
    private String message;
    private Map<String, String> errors;

    public ApiErrorResponseDTO() {}

    public ApiErrorResponseDTO(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public ApiErrorResponseDTO(String code, String message, Map<String, String> errors) {
        this.code = code;
        this.message = message;
        this.errors = errors;
    }

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public Map<String, String> getErrors() {
        return errors;
    }
}
