package it.fullstack.sdk.core.service;

import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.function.Function;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.transaction.annotation.Transactional;

import it.fullstack.sdk.core.service.filter.Filter;
import it.fullstack.sdk.core.service.filter.RangeFilter;
import it.fullstack.sdk.core.service.filter.StringFilter;
import jakarta.persistence.criteria.CriteriaBuilder.In;
import jakarta.persistence.criteria.Expression;
import jakarta.persistence.criteria.Root;
import jakarta.persistence.criteria.SetJoin;
import jakarta.persistence.metamodel.SetAttribute;
import jakarta.persistence.metamodel.SingularAttribute;

/**
 * Base service for constructing and executing complex queries.
 *
 * @param <T> the type of the entity which is queried.
 */
@Transactional(readOnly = true)
public abstract class QueryService<T> {
  
    protected <X> Specification<T> equal(X filterValue, SingularAttribute<? super T, X> field) {
      return buildSpecification(new Filter<X>().setEquals(filterValue), field);
    }
  
    protected <X> Specification<T> equal(X filterValue, Function<Root<T>, Expression<X>> metaclassFunction) {
      return buildSpecification(new Filter<X>().setEquals(filterValue), metaclassFunction);
    }  

    protected <X> Specification<T> notEqual(X filterValue, SingularAttribute<? super T, X> field) {
      return buildSpecification(new Filter<X>().setNotEquals(filterValue), field);
    }
  
    protected <X> Specification<T> in(List<X> filterValue, SingularAttribute<? super T, X> field) {
      return buildSpecification(new Filter<X>().setIn(filterValue), field);
    }
    
    protected <X> Specification<T> in(List<X> filterValue, Function<Root<T>, Expression<X>> metaclassFunction) {
      return buildSpecification(new Filter<X>().setIn(filterValue), metaclassFunction);
    }    
  
    protected <X extends Comparable<? super X>> Specification<T> lessThanOrEqualTo(X filterValue, SingularAttribute<? super T, X> field) {
      return buildRangeSpecification(new RangeFilter<X>().setLessThanOrEqual(filterValue), field);
    }
  
    protected <X extends Comparable<? super X>> Specification<T> lessThan(X filterValue, SingularAttribute<? super T, X> field) {
      return buildRangeSpecification(new RangeFilter<X>().setLessThan(filterValue), field);
    }
  
    protected <X extends Comparable<? super X>> Specification<T> greaterThanOrEqualTo(X filterValue, SingularAttribute<? super T, X> field) {
      return buildRangeSpecification(new RangeFilter<X>().setLessThanOrEqual(filterValue), field);
    }
  
    protected <X extends Comparable<? super X>> Specification<T> greaterThan(X filterValue, SingularAttribute<? super T, X> field) {
      return buildRangeSpecification(new RangeFilter<X>().setGreaterThan(filterValue), field);
    }
  
    protected <X> Specification<T> like(String filterValue, Function<Root<T>, Expression<String>> metaclassFunction) {
      return buildSpecification(new StringFilter().setContains(filterValue), metaclassFunction);
    }
  
    protected <X> Specification<T> like(String filterValue, SingularAttribute<? super T, String> field) {
      return buildStringSpecification(new StringFilter().setContains(filterValue), field);
    }
    
    protected <X> Specification<T> starts(String filterValue, Function<Root<T>, Expression<String>> metaclassFunction) {
      return buildSpecification(new StringFilter().setStarts(filterValue), metaclassFunction);
    }
  
    protected <X> Specification<T> starts(String filterValue, SingularAttribute<? super T, String> field) {
      return buildStringSpecification(new StringFilter().setStarts(filterValue), field);
    } 
    
    protected <X> Specification<T> ends(String filterValue, Function<Root<T>, Expression<String>> metaclassFunction) {
      return buildSpecification(new StringFilter().setEnds(filterValue), metaclassFunction);
    }
  
    protected <X> Specification<T> ends(String filterValue, SingularAttribute<? super T, String> field) {
      return buildStringSpecification(new StringFilter().setEnds(filterValue), field);
    }     
  


    /**
     * Helper function to return a specification for filtering on a single field, where equality, and null/non-null
     * conditions are supported.
     *
     * @param filter the individual attribute filter coming from the frontend.
     * @param field  the JPA static metamodel representing the field.
     * @param <X>    The type of the attribute which is filtered.
     * @return a Specification
     */
    protected <X> Specification<T> buildSpecification(Filter<X> filter, SingularAttribute<? super T, X> field) {
        return buildSpecification(filter, root -> root.get(field));
    }

    /**
     * Helper function to return a specification for filtering on a single field, where equality, and null/non-null
     * conditions are supported.
     *
     * @param filter            the individual attribute filter coming from the frontend.
     * @param metaclassFunction the function, which navigates from the current entity to a column, for which the filter applies.
     * @param <X>               The type of the attribute which is filtered.
     * @return a Specification
     */
    protected <X> Specification<T> buildSpecification(Filter<X> filter, Function<Root<T>, Expression<X>> metaclassFunction) {
        if (filter.getEquals() != null) return equalsSpecification(metaclassFunction, filter.getEquals());
        else if (filter.getIn() != null) return valueIn(metaclassFunction, filter.getIn());
        else if (filter.getNotIn() != null) return valueNotIn(metaclassFunction, filter.getNotIn());
        else if (filter.getNotEquals() != null) return notEqualsSpecification(metaclassFunction, filter.getNotEquals());
        else if (filter.getSpecified() != null) return byFieldSpecified(metaclassFunction, filter.getSpecified());

        return null;
    }

    /**
     * Helper function to return a specification for filtering on a {@link java.lang.String} field, where equality, containment,
     * and null/non-null conditions are supported.
     *
     * @param filter the individual attribute filter coming from the frontend.
     * @param field  the JPA static metamodel representing the field.
     * @return a Specification
     */
    protected Specification<T> buildStringSpecification(StringFilter filter, SingularAttribute<? super T, String> field) {
        return buildSpecification(filter, root -> root.get(field));
    }

    /**
     * Helper function to return a specification for filtering on a {@link java.lang.String} field, where equality, containment,
     * and null/non-null conditions are supported.
     *
     * @param filter            the individual attribute filter coming from the frontend.
     * @param metaclassFunction lambda, which based on a Root&lt;ENTITY&gt; returns Expression - basicaly picks a column
     * @return a Specification
     */
    protected Specification<T> buildSpecification(StringFilter filter, Function<Root<T>, Expression<String>> metaclassFunction) {
      if (filter.getEquals() != null) return equalsSpecification(metaclassFunction, filter.getEquals());
      else if (filter.getIn() != null) return valueIn(metaclassFunction, filter.getIn());
      else if (filter.getNotIn() != null) return valueNotIn(metaclassFunction, filter.getNotIn());
      else if (filter.getContains() != null) return likeUpperSpecification(metaclassFunction, filter.getContains());
      else if (filter.getStarts() != null) return startsUpperSpecification(metaclassFunction, filter.getStarts());            
      else if (filter.getEnds() != null) return endsUpperSpecification(metaclassFunction, filter.getEnds());
      else if (filter.getDoesNotContain() != null) return doesNotContainSpecification(metaclassFunction, filter.getDoesNotContain());
      else if (filter.getNotEquals() != null) return notEqualsSpecification(metaclassFunction, filter.getNotEquals());
      else if (filter.getSpecified() != null) return byFieldSpecified(metaclassFunction, filter.getSpecified());
      
      return null;
    }

    /**
     * Helper function to return a specification for filtering on a single {@link java.lang.Comparable}, where equality, less
     * than, greater than and less-than-or-equal-to and greater-than-or-equal-to and null/non-null conditions are
     * supported.
     *
     * @param <X>    The type of the attribute which is filtered.
     * @param filter the individual attribute filter coming from the frontend.
     * @param field  the JPA static metamodel representing the field.
     * @return a Specification
     */
    protected <X extends Comparable<? super X>> Specification<T> buildRangeSpecification(RangeFilter<X> filter, SingularAttribute<? super T, X> field) {
        return buildSpecification(filter, root -> root.get(field));
    }

    /**
     * Helper function to return a specification for filtering on a single {@link java.lang.Comparable}, where equality, less
     * than, greater than and less-than-or-equal-to and greater-than-or-equal-to and null/non-null conditions are
     * supported.
     *
     * @param <X>               The type of the attribute which is filtered.
     * @param filter            the individual attribute filter coming from the frontend.
     * @param metaclassFunction lambda, which based on a Root&lt;ENTITY&gt; returns Expression - basicaly picks a column
     * @return a Specification
     */
    protected <X extends Comparable<? super X>> Specification<T> buildSpecification(RangeFilter<X> filter, Function<Root<T>, Expression<X>> metaclassFunction) {
        if (filter.getEquals() != null) return equalsSpecification(metaclassFunction, filter.getEquals());
        else if (filter.getIn() != null) return valueIn(metaclassFunction, filter.getIn());
        else if (filter.getSpecified() != null) return byFieldSpecified(metaclassFunction, filter.getSpecified());
        else if (filter.getNotEquals() != null) return notEqualsSpecification(metaclassFunction, filter.getNotEquals());
        else if (filter.getNotIn() != null) return valueNotIn(metaclassFunction, filter.getNotIn());
        else if (filter.getGreaterThan() != null) return greaterThan(metaclassFunction, filter.getGreaterThan());
        else if (filter.getGreaterThanOrEqual() != null) return greaterThanOrEqualTo(metaclassFunction, filter.getGreaterThanOrEqual());
        else if (filter.getLessThan() != null) return lessThan(metaclassFunction, filter.getLessThan());
        else if (filter.getLessThanOrEqual() != null) return lessThanOrEqualTo(metaclassFunction, filter.getLessThanOrEqual());
        
        return null;
    }

    /**
     * Helper function to return a specification for filtering on one-to-one or many-to-one reference. Usage:
     * <pre>
     *   Specification&lt;Employee&gt; specByProjectId = buildReferringEntitySpecification(criteria.getProjectId(),
     * Employee_.project, Project_.id);
     *   Specification&lt;Employee&gt; specByProjectName = buildReferringEntitySpecification(criteria.getProjectName(),
     * Employee_.project, Project_.name);
     * </pre>
     *
     * @param filter     the filter object which contains a value, which needs to match or a flag if nullness is
     *                   checked.
     * @param reference  the attribute of the static metamodel for the referring entity.
     * @param valueField the attribute of the static metamodel of the referred entity, where the equality should be
     *                   checked.
     * @param <OTHER>    The type of the referenced entity.
     * @param <X>        The type of the attribute which is filtered.
     * @return a Specification
     */
    protected <OTHER, X> Specification<T> buildReferringEntitySpecification(Filter<X> filter,
                                                                                 SingularAttribute<? super T, OTHER> reference,
                                                                                 SingularAttribute<? super OTHER, X> valueField) {
        return buildSpecification(filter, root -> root.get(reference).get(valueField));
    }

    /**
     * Helper function to return a specification for filtering on one-to-many or many-to-many reference. Usage:
     * <pre>
     *   Specification&lt;Employee&gt; specByEmployeeId = buildReferringEntitySpecification(criteria.getEmployeId(),
     * Project_.employees, Employee_.id);
     *   Specification&lt;Employee&gt; specByEmployeeName = buildReferringEntitySpecification(criteria.getEmployeName(),
     * Project_.project, Project_.name);
     * </pre>
     *
     * @param filter     the filter object which contains a value, which needs to match or a flag if emptiness is
     *                   checked.
     * @param reference  the attribute of the static metamodel for the referring entity.
     * @param valueField the attribute of the static metamodel of the referred entity, where the equality should be
     *                   checked.
     * @param <OTHER>    The type of the referenced entity.
     * @param <X>        The type of the attribute which is filtered.
     * @return a Specification
     */
    protected <OTHER, X> Specification<T> buildReferringEntitySpecification(Filter<X> filter,
                                                                                 SetAttribute<T, OTHER> reference,
                                                                                 SingularAttribute<OTHER, X> valueField) {
        return buildReferringEntitySpecification(filter, root -> root.join(reference), entity -> entity.get(valueField));
    }

    /**
     * Helper function to return a specification for filtering on one-to-many or many-to-many reference.Usage:<pre>
     *   Specification&lt;Employee&gt; specByEmployeeId = buildReferringEntitySpecification(
     *          criteria.getEmployeId(),
     *          root -&gt; root.get(Project_.company).join(Company_.employees),
     *          entity -&gt; entity.get(Employee_.id));
     *   Specification&lt;Employee&gt; specByProjectName = buildReferringEntitySpecification(
     *          criteria.getProjectName(),
     *          root -&gt; root.get(Project_.project)
     *          entity -&gt; entity.get(Project_.name));
     * </pre>
     *
     * @param filter           the filter object which contains a value, which needs to match or a flag if emptiness is
     *                         checked.
     * @param functionToEntity the function, which joins he current entity to the entity set, on which the filtering is applied.
     * @param entityToColumn   the function, which of the static metamodel of the referred entity, where the equality should be
     *                         checked.
     * @param <OTHER>          The type of the referenced entity.
     * @param <MISC>           The type of the entity which is the last before the OTHER in the chain.
     * @param <X>              The type of the attribute which is filtered.
     * @return a Specification
     */
    protected <OTHER, MISC, X> Specification<T> buildReferringEntitySpecification(Filter<X> filter,
                                                                                       Function<Root<T>, SetJoin<MISC, OTHER>> functionToEntity,
                                                                                       Function<SetJoin<MISC, OTHER>, Expression<X>> entityToColumn) {
        if (filter.getEquals() != null) {
            return equalsSpecification(functionToEntity.andThen(entityToColumn), filter.getEquals());
        } else if (filter.getSpecified() != null) {
            // Interestingly, 'functionToEntity' doesn't work, we need the longer lambda formula
            return byFieldSpecified(root -> functionToEntity.apply(root), filter.getSpecified());
        }
        return null;
    }

    /**
     * Helper function to return a specification for filtering on one-to-many or many-to-many reference.Where equality, less
     * than, greater than and less-than-or-equal-to and greater-than-or-equal-to and null/non-null conditions are
     * supported. Usage:
     * <pre>
     *   Specification&lt;Employee&gt; specByEmployeeId = buildReferringEntitySpecification(criteria.getEmployeId(),
     * Project_.employees, Employee_.id);
     *   Specification&lt;Employee&gt; specByEmployeeName = buildReferringEntitySpecification(criteria.getEmployeName(),
     * Project_.project, Project_.name);
     * </pre>
     *
     * @param <X>        The type of the attribute which is filtered.
     * @param filter     the filter object which contains a value, which needs to match or a flag if emptiness is
     *                   checked.
     * @param reference  the attribute of the static metamodel for the referring entity.
     * @param valueField the attribute of the static metamodel of the referred entity, where the equality should be
     *                   checked.
     * @param <OTHER>    The type of the referenced entity.
     * @return a Specification
     */
    protected <OTHER, X extends Comparable<? super X>> Specification<T> buildReferringEntitySpecification(final RangeFilter<X> filter,
                                                                                                               final SetAttribute<T, OTHER> reference,
                                                                                                               final SingularAttribute<OTHER, X> valueField) {
        return buildReferringEntitySpecification(filter, root -> root.join(reference), entity -> entity.get(valueField));
    }

    /**
     * Helper function to return a specification for filtering on one-to-many or many-to-many reference.Where equality, less
 than, greater than and less-than-or-equal-to and greater-than-or-equal-to and null/non-null conditions are
 supported. Usage:
 <pre><code>
     *   Specification&lt;Employee&gt; specByEmployeeId = buildReferringEntitySpecification(
     *          criteria.getEmployeId(),
     *          root -&gt; root.get(Project_.company).join(Company_.employees),
     *          entity -&gt; entity.get(Employee_.id));
     *   Specification&lt;Employee&gt; specByProjectName = buildReferringEntitySpecification(
     *          criteria.getProjectName(),
     *          root -&gt; root.get(Project_.project)
     *          entity -&gt; entity.get(Project_.name));
     * </code>
     * </pre>
     *
     * @param <X>              The type of the attribute which is filtered.
     * @param filter           the filter object which contains a value, which needs to match or a flag if emptiness is
     *                         checked.
     * @param functionToEntity the function, which joins he current entity to the entity set, on which the filtering is applied.
     * @param entityToColumn   the function, which of the static metamodel of the referred entity, where the equality should be
     *                         checked.
     * @param <OTHER>          The type of the referenced entity.
     * @param <MISC>           The type of the entity which is the last before the OTHER in the chain.
     * @return a Specification
     */
    protected <OTHER, MISC, X extends Comparable<? super X>> Specification<T> buildReferringEntitySpecification(final RangeFilter<X> filter,
                                                                                                                     Function<Root<T>, SetJoin<MISC, OTHER>> functionToEntity,
                                                                                                                     Function<SetJoin<MISC, OTHER>, Expression<X>> entityToColumn) {

        Function<Root<T>, Expression<X>> fused = functionToEntity.andThen(entityToColumn);
        if (filter.getEquals() != null) return equalsSpecification(fused, filter.getEquals());
        else if (filter.getIn() != null) return valueIn(fused, filter.getIn());
        else if (filter.getSpecified() != null) return byFieldSpecified(root -> functionToEntity.apply(root), filter.getSpecified());
        else if (filter.getNotEquals() != null) return notEqualsSpecification(fused, filter.getNotEquals());
        else if (filter.getNotIn() != null) return valueNotIn(fused, filter.getNotIn());
        else if (filter.getGreaterThan() != null) return greaterThan(fused, filter.getGreaterThan());
        else if (filter.getGreaterThanOrEqual() != null) return greaterThanOrEqualTo(fused, filter.getGreaterThanOrEqual());
        else if (filter.getLessThan() != null) return lessThan(fused, filter.getLessThan());
        else if (filter.getLessThanOrEqual() != null) return lessThanOrEqualTo(fused, filter.getLessThanOrEqual());
        
        return null;
    }

    /**
     * Generic method, which based on a Root&lt;ENTITY&gt; returns an Expression which type is the same as the given 'value' type.
     *
     * @param metaclassFunction function which returns the column which is used for filtering.
     * @param value             the actual value to filter for.
     * @param <X>              The type of the attribute which is filtered.
     * @return a Specification.
     */
    protected <X> Specification<T> equalsSpecification(Function<Root<T>, Expression<X>> metaclassFunction, final X value) {
        return (root, query, builder) -> builder.equal(metaclassFunction.apply(root), value);
    }

    /**
     * Generic method, which based on a Root&lt;ENTITY&gt; returns an Expression which type is the same as the given 'value' type.
     *
     * @param metaclassFunction function which returns the column which is used for filtering.
     * @param value             the actual value to exclude for.
     * @param <X>              The type of the attribute which is filtered.
     * @return a Specification.
     */
    protected <X> Specification<T> notEqualsSpecification(Function<Root<T>, Expression<X>> metaclassFunction, final X value) {
        return (root, query, builder) -> builder.not(builder.equal(metaclassFunction.apply(root), value));
    }

    /**
     * <p>likeUpperSpecification.</p>
     *
     * @param metaclassFunction a {@link java.util.function.Function} object.
     * @param value a {@link java.lang.String} object.
     * @return a {@link org.springframework.data.jpa.domain.Specification} object.
     */
    protected Specification<T> likeUpperSpecification(Function<Root<T>, Expression<String>> metaclassFunction, final String value) {
        return (root, query, builder) -> builder.like(builder.upper(metaclassFunction.apply(root)), wrapLikeQuery(value));
    }
    
    /**
     * <p>startsUpperSpecification.</p>
     *
     * @param metaclassFunction a {@link java.util.function.Function} object.
     * @param value a {@link java.lang.String} object.
     * @return a {@link org.springframework.data.jpa.domain.Specification} object.
     */
    protected Specification<T> startsUpperSpecification(Function<Root<T>, Expression<String>> metaclassFunction, final String value) {
        return (root, query, builder) -> builder.like(builder.upper(metaclassFunction.apply(root)), wrapStartQuery(value));
    }
    
    /**
     * <p>endsUpperSpecification.</p>
     *
     * @param metaclassFunction a {@link java.util.function.Function} object.
     * @param value a {@link java.lang.String} object.
     * @return a {@link org.springframework.data.jpa.domain.Specification} object.
     */
    protected Specification<T> endsUpperSpecification(Function<Root<T>, Expression<String>> metaclassFunction, final String value) {
        return (root, query, builder) -> builder.like(builder.upper(metaclassFunction.apply(root)), wrapEndQuery(value));
    }    

    /**
     * <p>doesNotContainSpecification.</p>
     *
     * @param metaclassFunction a {@link java.util.function.Function} object.
     * @param value a {@link java.lang.String} object.
     * @return a {@link org.springframework.data.jpa.domain.Specification} object.
     */
    protected Specification<T> doesNotContainSpecification(Function<Root<T>, Expression<String>> metaclassFunction, final String value) {
        return (root, query, builder) -> builder.not(builder.like(builder.upper(metaclassFunction.apply(root)), wrapLikeQuery(value)));
    }

    /**
     * <p>byFieldSpecified.</p>
     *
     * @param metaclassFunction a {@link java.util.function.Function} object.
     * @param specified a boolean.
     * @param <X> a X object.
     * @return a {@link org.springframework.data.jpa.domain.Specification} object.
     */
    protected <X> Specification<T> byFieldSpecified(Function<Root<T>, Expression<X>> metaclassFunction, final boolean specified) {
        return specified ?
            (root, query, builder) -> builder.isNotNull(metaclassFunction.apply(root)) :
            (root, query, builder) -> builder.isNull(metaclassFunction.apply(root));
    }

    /**
     * <p>byFieldEmptiness.</p>
     *
     * @param metaclassFunction a {@link java.util.function.Function} object.
     * @param specified a boolean.
     * @param <X> a X object.
     * @return a {@link org.springframework.data.jpa.domain.Specification} object.
     */
    protected <X> Specification<T> byFieldEmptiness(Function<Root<T>, Expression<Set<X>>> metaclassFunction, final boolean specified) {
        return specified ?
            (root, query, builder) -> builder.isNotEmpty(metaclassFunction.apply(root)) :
            (root, query, builder) -> builder.isEmpty(metaclassFunction.apply(root));
    }

    /**
     * <p>valueIn.</p>
     *
     * @param metaclassFunction a {@link java.util.function.Function} object.
     * @param values a {@link java.util.Collection} object.
     * @param <X> a X object.
     * @return a {@link org.springframework.data.jpa.domain.Specification} object.
     */
    protected <X> Specification<T> valueIn(Function<Root<T>, Expression<X>> metaclassFunction, final Collection<X> values) {
        return (root, query, builder) -> {
            In<X> in = builder.in(metaclassFunction.apply(root));
            for (X value : values) {
                in = in.value(value);
            }
            return in;
        };
    }

    /**
     * <p>valueNotIn.</p>
     *
     * @param metaclassFunction a {@link java.util.function.Function} object.
     * @param values a {@link java.util.Collection} object.
     * @param <X> a X object.
     * @return a {@link org.springframework.data.jpa.domain.Specification} object.
     */
    protected <X> Specification<T> valueNotIn(Function<Root<T>, Expression<X>> metaclassFunction, final Collection<X> values) {
        return (root, query, builder) -> {
            In<X> in = builder.in(metaclassFunction.apply(root));
            for (X value : values) {
                in = in.value(value);
            }
            return builder.not(in);
        };
    }

    /**
     * <p>greaterThanOrEqualTo.</p>
     *
     * @param metaclassFunction a {@link java.util.function.Function} object.
     * @param value a X object.
     * @param <X> a X object.
     * @return a {@link org.springframework.data.jpa.domain.Specification} object.
     */
    protected <X extends Comparable<? super X>> Specification<T> greaterThanOrEqualTo(Function<Root<T>, Expression<X>> metaclassFunction, final X value) {
        return (root, query, builder) -> builder.greaterThanOrEqualTo(metaclassFunction.apply(root), value);
    }

    /**
     * <p>greaterThan.</p>
     *
     * @param metaclassFunction a {@link java.util.function.Function} object.
     * @param value a X object.
     * @param <X> a X object.
     * @return a {@link org.springframework.data.jpa.domain.Specification} object.
     */
    protected <X extends Comparable<? super X>> Specification<T> greaterThan(Function<Root<T>, Expression<X>> metaclassFunction, final X value) {
        return (root, query, builder) -> builder.greaterThan(metaclassFunction.apply(root), value);
    }

    /**
     * <p>lessThanOrEqualTo.</p>
     *
     * @param metaclassFunction a {@link java.util.function.Function} object.
     * @param value a X object.
     * @param <X> a X object.
     * @return a {@link org.springframework.data.jpa.domain.Specification} object.
     */
    protected <X extends Comparable<? super X>> Specification<T> lessThanOrEqualTo(Function<Root<T>, Expression<X>> metaclassFunction, final X value) {
        return (root, query, builder) -> builder.lessThanOrEqualTo(metaclassFunction.apply(root), value);
    }

    /**
     * <p>lessThan.</p>
     *
     * @param metaclassFunction a {@link java.util.function.Function} object.
     * @param value a X object.
     * @param <X> a X object.
     * @return a {@link org.springframework.data.jpa.domain.Specification} object.
     */
    protected <X extends Comparable<? super X>> Specification<T> lessThan(Function<Root<T>, Expression<X>> metaclassFunction, final X value) {
        return (root, query, builder) -> builder.lessThan(metaclassFunction.apply(root), value);
    }

    /**
     * <p>wrapLikeQuery.</p>
     *
     * @param txt a {@link java.lang.String} object.
     * @return a {@link java.lang.String} object.
     */
    protected String wrapLikeQuery(String txt) {
        return "%" + txt.toUpperCase() + '%';
    }
    
    /**
     * <p>wrapStartQuery.</p>
     *
     * @param txt a {@link java.lang.String} object.
     * @return a {@link java.lang.String} object.
     */
    protected String wrapStartQuery(String txt) {
        return txt.toUpperCase() + "%";
    }
    
    /**
     * <p>wrapEndQuery.</p>
     *
     * @param txt a {@link java.lang.String} object.
     * @return a {@link java.lang.String} object.
     */
    protected String wrapEndQuery(String txt) {
        return "%" + txt.toUpperCase();
    }    

}