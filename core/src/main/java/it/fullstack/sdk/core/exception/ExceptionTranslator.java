package it.fullstack.sdk.core.exception;

import java.util.ArrayList;
import java.util.Map;
import java.util.stream.Collectors;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

import it.fullstack.sdk.core.exception.domain.BadRequestException;
import it.fullstack.sdk.core.exception.domain.NotFoundException;
import jakarta.validation.ConstraintViolation;
import jakarta.validation.ConstraintViolationException;

@ControllerAdvice
@Slf4j
public class ExceptionTranslator {
    private static final String GENERIC_ERROR_CODE = "400_000000";

    /**
     * Lista delle eccezioni per le quali non dovrà essere scritto il log di errore.
     * Indicare la lista (separata da virgola) nell'application.properties; es:
     * ```
     * exception-translator:
     *   log-exemptions: it.fullstack.core.exception.domain.NotFoundException, it.fullstack.core.exception.domain.BadRequestException
     * ```
     *
     * oppure multilinea
     *
     * ```
     * exception-translator:
     *   log-exemptions: >
     *     it.fullstack.core.exception.domain.NotFoundException,
     *     it.fullstack.core.exception.domain.BadRequestException
     * ```
     *
     * è possibile indicare una wildcard per referenziare un package parent, es:
     * ```
     * it.fullstack.core.exception.*
     * ```
     * per indicare tutte le classi il cui package dipende da `it.fullstack.core.exception`
     */
    @Value("${exception-translator.log-exemptions:}")
    private ArrayList<String> logExemptions = new ArrayList<>();

    /**
     * Handler di fallback per tutte le eccezioni non gestite puntualmente
     */
    @ExceptionHandler
    public ResponseEntity<ApiErrorResponseDTO> handleThrowable(Throwable t, WebRequest request) {
        return processApiError(new ApiErrorResponseDTO(GENERIC_ERROR_CODE, t.getMessage()), HttpStatus.BAD_REQUEST, t);
    }

    @ExceptionHandler
    public ResponseEntity<ApiErrorResponseDTO> handleBadRequest(BadRequestException ex, WebRequest request) {
        return processApiError(new ApiErrorResponseDTO(ex.getCode(), ex.getMessage()), HttpStatus.BAD_REQUEST, ex);
    }

    @ExceptionHandler
    public ResponseEntity<ApiErrorResponseDTO> handleRuntimeException(RuntimeException ex, WebRequest request) {
        return processApiError(new ApiErrorResponseDTO(BadRequestException.CODE, ex.getMessage()), HttpStatus.BAD_REQUEST, ex);

    }

    @ExceptionHandler
    public ResponseEntity<ApiErrorResponseDTO> handleNotFound(jakarta.persistence.EntityNotFoundException ex, WebRequest request) {
        return processApiError(new ApiErrorResponseDTO(NotFoundException.CODE, ex.getMessage()), HttpStatus.NOT_FOUND, ex);
    }

    @ExceptionHandler
    public ResponseEntity<ApiErrorResponseDTO> handleNotFound(NotFoundException ex, WebRequest request) {
        return processApiError(new ApiErrorResponseDTO(ex.getCode(), ex.getMessage()), HttpStatus.NOT_FOUND, ex);
    }

    @ExceptionHandler
    public ResponseEntity<ApiErrorResponseDTO> handleConstraintViolation(ConstraintViolationException ex, WebRequest request) {
        Map<String, String> errors = ex.getConstraintViolations().stream().collect(
                Collectors.toMap(
                        x -> x.getPropertyPath().toString(),
                        ConstraintViolation::getMessage,
                        (existing, replacement) -> existing
                ));

        return processApiError(new ApiErrorResponseDTO(BadRequestException.CODE, ex.getMessage(), errors), HttpStatus.BAD_REQUEST, ex);
    }

    @ExceptionHandler
    public ResponseEntity<ApiErrorResponseDTO> handleConstraintViolation(MethodArgumentNotValidException ex, WebRequest request) {
        Map<String, String> errors = ex.getBindingResult().getAllErrors().stream().collect(
                Collectors.toMap(
                        x -> ((FieldError) x).getField(),
                        ObjectError::getDefaultMessage,
                        (existing, replacement) -> existing
                ));

        return processApiError(new ApiErrorResponseDTO(BadRequestException.CODE, ex.getMessage(), errors), HttpStatus.BAD_REQUEST, ex);
    }

    protected ResponseEntity<ApiErrorResponseDTO> processApiError(
            ApiErrorResponseDTO dto,
            HttpStatus httpStatus,
            Throwable throwable
    ) {
        if (!isLogExempted(throwable)) {
            log.error("Exception: {}", throwable.getMessage(), throwable);
        }

        return ResponseEntity
                .status(httpStatus)
                .body(dto);
    }

    private boolean isLogExempted(Throwable throwable) {
        String throwableClassName = throwable.getClass().getName();
        for (String logExemption: logExemptions) {
            if (null == logExemption || logExemption.isEmpty()) {
                // Pattern non applicabile
                continue;
            } else if (logExemption.endsWith("*") && throwableClassName.startsWith(logExemption.substring(0, logExemption.length() - 1))) {
                // Match wildcard
                return true;
            } else if(throwableClassName.equals(logExemption)) {
                // Match puntuale
                return true;
            }
        }

        return false;
    }
}
